import java.util.ArrayList;
public class BinaryTree extends AbstractBinaryTree{     
//--------------------------------------DEPTH----------------------------------------------------------------
//--------------------------------------DEPTH----------------------------------------------------------------
  /** compute the depth of a node */
  @Override
  public int depth(Node node){
    int depth = 0;
    Node thisOne = node;
    if(thisOne == null){
      return(-1);
    }
    if(find(node.getData()) == false){
      return(-1);
    }
    while(thisOne != this.root){
      depth++;
      thisOne = thisOne.getParent();
    }   
    return(depth);
  }
  
//---------------------------------FIND-----------------------------------------------------------------
//---------------------------------FIND-----------------------------------------------------------------
  /** Check if a number is in the tree or not */
  @Override
  public boolean find(Integer i){
    Node move = root;
    boolean check = false;
    check = myFind(i,check,move);
    return(check);
  }
  
  public boolean myFind(int i , boolean state, Node root){
    if(root.getData() == i){
      state = true;
      return( state);
    } 
    if(root.getLeft()!= null){
      if(myFind(i,state,root.getLeft()) == true){
        return(true);
      }
    }
    if(root.getRight()!= null){
      if(myFind(i,state,root.getRight()) == true){
        return(true);
      }  
    }
    return(state);
  }
//---------------------------------GET-NUMBER-----------------------------------------------------------------
//---------------------------------GET-NUMBER-----------------------------------------------------------------
  /** Create a list of all the numbers in the tree. */
  /*  If a number appears N times in the tree then this */
  /*  number should appear N times in the returned list */ 
  @Override
  public ArrayList<Integer> getNumbers(){ 
    ArrayList<Integer>  list = new ArrayList<Integer>();
    Node movingNode = this.root;
    list = makeList(movingNode);
    return(list);
  }
  public ArrayList<Integer> makeList(Node movingNode){
    ArrayList<Integer> list = new ArrayList<Integer>();
    if(movingNode.getRight() != null){
      list.addAll(makeList(movingNode.getRight()));
    }
    if (movingNode.getLeft() != null) {
      list.addAll(makeList(movingNode.getLeft()));
    }
    list.add(movingNode.getData());
    return(list);
    
  }

//---------------------------------ADD_LEAF/REMOVE_LEAF-----------------------------------------------------------------
//---------------------------------ADD_LEAF/REMOVE_LEAF-----------------------------------------------------------------
  /** Adds a leaf to the tree with number specifed by input.  */
  @Override
  public void addLeaf(Integer i){
    if(this.root == null){
      this.root = new Node(i);
    }
    else if(this.root != null){
      Node root = this.root;
      Node addNode = findLeafNode(root);
      //System.out.println(addNode.getData());
      Node newNode = new Node(i);
      addNode.setLeft(newNode);
      newNode.setParent(addNode);
      this.sizeOfTree++;
    }
  }
//----------------------------------------------------------
  /** Removes "some" leaf from the tree.       */
  /*  If the tree is empty should return null  */
  @Override
  public Node removeLeaf(){
    this.sizeOfTree--;
    if(sizeOfTree <=0){
      sizeOfTree = 0;
      this.root = null;
      return(null);
    }
    Node position = findLeafNode(this.root);
    Node positionParent = position.getParent();
    if(positionParent.getLeft() == position){
      positionParent.setLeft(null);
    }
    if(positionParent.getRight() == position){
      positionParent.setRight(null);
    }
    position.setParent(null);
    return(position);
  }
  
  //Finds Leaf
  public Node findLeafNode(Node root){
    if(root.getLeft() != null) {
      root = findLeafNode(root.getLeft());
    }
    if(root.getRight() != null) {
      root = findLeafNode(root.getRight());
    }
    if((root.getLeft() == null) && (root.getRight() == null) ) {
      return(root);
    }
    return(root);
  }
}


 
