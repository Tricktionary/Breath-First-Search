public class Node{
  protected Integer data;
  
  protected Node left;
  protected Node right;
  //Adding a parent node
  protected Node parent;
  
  public Node(Integer data){
    this.data = data;
    //Adding parent
    this.left = this.right = this.parent = null;
  }
  
  public Node(Integer data, Node left, Node right){
    this.data = data;
    this.left = left;
    this.right = right;
    this.parent = null;
  }
  
  public Integer getData(){ return this.data; }
  public Node    getLeft(){ return this.left; }
  public Node    getRight(){ return this.right; }
  public Node    getParent(){ return this.parent;}
  
  public void setLeft(Node left){ this.left = left; }
  public void setRight(Node right){ this.right = right;}
  public void setParent(Node parent){ this.parent = parent;}

}
